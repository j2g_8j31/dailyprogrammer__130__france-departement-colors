#!/usr/bin/python3

import sys

totalDepartements = int(sys.stdin.readline().rstrip('\n'))
Departements = {}
colors = {}

for i in range(totalDepartements):

    line = sys.stdin.readline().rstrip('\n').split(' ')

    departement = line[0]
    Departements[departement] = {}
    colors[departement] = {}

    color = 0
    for adjacency in line[1:]:

        ## check each adjacency for existence in colors{} with current color 'color'
        ## if found, increment color and go to next adjacency
        if adjacency in colors:
            if color == colors[adjacency]:
                color += 1

        colors[departement] = color

    print(departement, color)
